#!/bin/bash

cat system/system/priv-app/Gallery_OCR_11.1/Gallery_OCR_11.1.apk.* 2>/dev/null >> system/system/priv-app/Gallery_OCR_11.1/Gallery_OCR_11.1.apk
rm -f system/system/priv-app/Gallery_OCR_11.1/Gallery_OCR_11.1.apk.* 2>/dev/null
cat system/system/priv-app/GameSpace-nubia/GameSpace-nubia.apk.* 2>/dev/null >> system/system/priv-app/GameSpace-nubia/GameSpace-nubia.apk
rm -f system/system/priv-app/GameSpace-nubia/GameSpace-nubia.apk.* 2>/dev/null
cat system/system/priv-app/ZTE_Camera/ZTE_Camera.apk.* 2>/dev/null >> system/system/priv-app/ZTE_Camera/ZTE_Camera.apk
rm -f system/system/priv-app/ZTE_Camera/ZTE_Camera.apk.* 2>/dev/null
cat system/system/priv-app/Wallet/Wallet.apk.* 2>/dev/null >> system/system/priv-app/Wallet/Wallet.apk
rm -f system/system/priv-app/Wallet/Wallet.apk.* 2>/dev/null
cat system/system/priv-app/PhotoEditor_MFV/PhotoEditor_MFV.apk.* 2>/dev/null >> system/system/priv-app/PhotoEditor_MFV/PhotoEditor_MFV.apk
rm -f system/system/priv-app/PhotoEditor_MFV/PhotoEditor_MFV.apk.* 2>/dev/null
cat system/system/lib64/libzteFaceRecognitionModels_arm64-v8a.so.* 2>/dev/null >> system/system/lib64/libzteFaceRecognitionModels_arm64-v8a.so
rm -f system/system/lib64/libzteFaceRecognitionModels_arm64-v8a.so.* 2>/dev/null
cat system/system/app/ThemeResource_A30/ThemeResource_A30.apk.* 2>/dev/null >> system/system/app/ThemeResource_A30/ThemeResource_A30.apk
rm -f system/system/app/ThemeResource_A30/ThemeResource_A30.apk.* 2>/dev/null
cat system/system/app/LiveWallpaper_Axon_1080_2400_c1gc2gc3k123/LiveWallpaper_Axon_1080_2400_c1gc2gc3k123.apk.* 2>/dev/null >> system/system/app/LiveWallpaper_Axon_1080_2400_c1gc2gc3k123/LiveWallpaper_Axon_1080_2400_c1gc2gc3k123.apk
rm -f system/system/app/LiveWallpaper_Axon_1080_2400_c1gc2gc3k123/LiveWallpaper_Axon_1080_2400_c1gc2gc3k123.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat vendor_bootimg/12_dtbdump_(W)H&.dtb.* 2>/dev/null >> vendor_bootimg/12_dtbdump_(W)H&.dtb
rm -f vendor_bootimg/12_dtbdump_(W)H&.dtb.* 2>/dev/null
cat system_ext/partner-app/Weibo/Weibo.apk.* 2>/dev/null >> system_ext/partner-app/Weibo/Weibo.apk
rm -f system_ext/partner-app/Weibo/Weibo.apk.* 2>/dev/null
cat system_ext/partner-app/moffice/moffice.apk.* 2>/dev/null >> system_ext/partner-app/moffice/moffice.apk
rm -f system_ext/partner-app/moffice/moffice.apk.* 2>/dev/null
cat system_ext/partner-app/baidusearch/baidusearch.apk.* 2>/dev/null >> system_ext/partner-app/baidusearch/baidusearch.apk
rm -f system_ext/partner-app/baidusearch/baidusearch.apk.* 2>/dev/null
cat system_ext/partner-app/weixin/weixin.apk.* 2>/dev/null >> system_ext/partner-app/weixin/weixin.apk
rm -f system_ext/partner-app/weixin/weixin.apk.* 2>/dev/null
cat system_ext/partner-app/Ctrip/Ctrip.apk.* 2>/dev/null >> system_ext/partner-app/Ctrip/Ctrip.apk
rm -f system_ext/partner-app/Ctrip/Ctrip.apk.* 2>/dev/null
cat system_ext/priv-app/Settings_MFV/Settings_MFV.apk.* 2>/dev/null >> system_ext/priv-app/Settings_MFV/Settings_MFV.apk
rm -f system_ext/priv-app/Settings_MFV/Settings_MFV.apk.* 2>/dev/null
cat system_ext/priv-app/SystemUI_MFV/SystemUI_MFV.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI_MFV/SystemUI_MFV.apk
rm -f system_ext/priv-app/SystemUI_MFV/SystemUI_MFV.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
